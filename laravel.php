<?php

 public function PorDia()
    {

        $a = DB::table('solicitudes_de_prestamos')
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))
            ->groupBy('date')
            ->orderBy('date', 'desc')
            ->get();

        return view('reportes.por-dia', ['model' => $a]);

    }
 

function calculeDay($date, $hasta)
{

    $datework = Carbon::parse($date->toDateString());
    $now      = Carbon::parse($hasta->toDateString());
    return $datework->diffInDays($now);



  $whereGpub = [];

        if ($this->fill_gpub_link_id) {
            $whereGpub['tracking_id'] = $this->fill_gpub_link_id;
        }




 public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i:s');
    }


  public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%' . $value . '%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%' . $value . '%');
    }
    //$result = BookingDates::whereLike('email', $email)->orWhereLike('name', $name)->get();
} 

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:Super|Autorizadores|Administradores|Cuota|Dashboard|Legal|Partner']], function () {
