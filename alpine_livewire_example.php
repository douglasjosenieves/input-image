 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


  <link rel="stylesheet" href="/assets/back/assets/styles.css" />
  <link rel="stylesheet" href="/assets/back/assets/styles2.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css" integrity="sha512-Cv93isQdFwaKBV+Z4X8kaVBYWHST58Xb/jVOcV9aRsGSArZsgAnFIhMpDoMDcFNoUtday1hdjn0nGp3+KZyyFw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="/assets/js/alpinejs.min.js" defer></script>
  <script src="/assets/js/axios.min.js" defer></script>
   
 
  <link href="/../plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
  <link href="/../plugins/animate/animate.css" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.12/css/intlTelInput.min.css"
  integrity="sha512-yye/u0ehQsrVrfSd6biT17t39Rg9kNc+vENcCXZuMz2a+LWFGvXUnYuWUW6pbfYj1jcBb/C39UZw2ciQvwDDvg=="
  crossorigin="anonymous" referrerpolicy="no-referrer" />

  <link href="/assets/plugins/validator-pass/style.css" rel="stylesheet" type="text/css">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css'> 
 


  <script src="/../plugins/sweet-alert2/sweetalert2.min.js"></script>
   <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>

  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.12/js/intlTelInput.min.js"
  integrity="sha512-OnkjbJ4TwPpgSmjXACCb5J4cJwi880VRe+vWpPDlr8M38/L3slN5uUAeOeWU2jN+4vN0gImCXFGdJmc0wO4Mig=="
  crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.12/js/utils.min.js"
  integrity="sha512-JtpGKUjQUqCJmgw3TOr8bkL8QqB8oCbofRU39xgDnRYTfIDvfg0gQs2efeKxBCdwSAP/h3CSqhn87+giWSsxjw=="
  crossorigin="anonymous" referrerpolicy="no-referrer"></script>  
<div >
     <div x-data="main_account()">
    <div class="row">

        {{-- ---------------------------------------------------------------------- --}}
        {{--                                  Left                                  --}}
        {{-- ---------------------------------------------------------------------- --}}


        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="card-title font-weight-bold">Perfil del anunciante</div>

                    <div class="d-flex justify-content-between">
                        <div>
                            
                            <label for="image">   
                            <img style="cursor: pointer;" class="rounded-circle object-fit-cover" width="50" height="50"
                                src="{{ $img_user }}" />
                                <input id="image" class="d-none" type="file" wire:model="image">
                                <div class="text-primary font-weight-bold" wire:loading wire:target="image">Uploading...</div>

                            </label>
                            @error('image') <div><small class="text-danger font-weight-bold">{{ $message }}</small> </div>@enderror

                            
                         </div>

                                
 

                        <div>

                            <div class="d-flex justify-content-between">
                                <div class="mr-4">@lang('Perfil')</div>
                                <div>{{ porcentajePerfil() }}%</div>
                            </div>
                            <gl-progress-bar _ngcontent-pda-c85="" class="w-100 mt-2" _nghost-pda-c84="">
                                <div _ngcontent-pda-c84="" class="bg-primary-200 progress-bar-container">
                                    <div _ngcontent-pda-c84="" class="bg-primary progress-bar-track" style="width: {{ porcentajePerfil() }}%">
                                    </div>
                                </div>
                            </gl-progress-bar>

                            <button onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                class="btn btn-primary mt-2" type="button"> @lang('Cerrar Sesión')</button>
                        </div>

                    </div>

                </div>
            </div>



            


            <div class="card mt-3">
                <div class="card-body">
                    <div class="card-title font-weight-bold">@lang('Usuario de acceso') </div>

                    <div class="form-group">
                        <input disabled id="user_accss" type="text" placeholder="@lang('Usuario')"
                            class="form-control   @error('user_accss') is-invalid @enderror" id="user_accss"
                            value="{{ Auth::user()->email }}" name="user_accss" required>
                        @error('user_accss') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                </div>
            </div>


            <div class="card mt-3">
                <div class="card-body">
                    <div class="card-title font-weight-bold">@lang('Cambiar contraseña') </div>

                    <form  wire:submit.prevent="chengePass(Object.fromEntries(new FormData(event.target)))"
                        autocomplete="off">




                        <div class="form-group">
                            <label for="current_password">@lang('Contraseña actual')</label>
                            <input id="current_password" wire:model.defer="current_password" type="password"
                                placeholder="" class="form-control @error('current_password') is-invalid @enderror"
                                id="current_password" name="current_password" value="{{ old('current_password') }}"
                                required>
                            @error('current_password') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>

                        <div class="form-group password-strength">
                            <label for="password-input">@lang('Nueva contraseña')</label>
                            <div class="input-group">
                                <input class="password-strength__input form-control" wire:model.defer="new_password"
                                    type="password" id="password-input" aria-describedby="passwordHelp"
                                    placeholder="Enter password" @error('new_password') is-invalid @enderror"
                                    id="new_password" name="new_password" value="{{ old('new_password') }}" required />
                                @error('new_confirm_password') <span class="text-danger">{{ $message }}</span> @enderror

                                <div class="input-group-append">
                                    <button class="password-strength__visibility btn btn-outline-secondary"
                                        type="button"><span class="password-strength__visibility-icon"
                                            data-visible="hidden"><i class="fas fa-eye-slash"></i></span><span
                                            class="password-strength__visibility-icon js-hidden"
                                            data-visible="visible"><i class="fas fa-eye"></i></span></button>
                                </div>
                            </div><small class="password-strength__error text-danger js-hidden">This symbol is not
                                allowed!</small><small class="form-text text-muted mt-2" id="passwordHelp">Add 9
                                charachters or more, lowercase letters, uppercase
                                letters, numbers and symbols to make the password really strong!</small>
                        </div>
                        <div class="password-strength__bar-block progress mb-4">
                            <div class="password-strength__bar progress-bar bg-danger" role="progressbar"
                                aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        

                        <div class="form-group">
                            <label for="new_confirm_password">@lang('Confirmar contraseña')</label>
                            <input id="new_confirm_password" wire:model.defer="new_confirm_password" type="password"
                                placeholder="" class="form-control @error('new_confirm_password') is-invalid @enderror"
                                id="new_confirm_password" name="new_confirm_password"
                                value="{{ old('new_confirm_password') }}" required>
                            @error('new_confirm_password') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>


                        <div class="form-group text-right">
                            <button type="submit" wire:loading.attr="disabled"
                                class="btn btn-outline-primary btn-block">

                                <div wire:loading>
                                    <span class="spinner-border spinner-border-sm" role="status"
                                        aria-hidden="true"></span>
                                    Loading...
                                </div>

                                <div wire:loading.remove> @lang('Cambiar contraseña')</div>
                            </button>
                        </div>

                    </form>

                </div>

            </div>

        </div>

        {{-- ---------------------------------------------------------------------- --}}
        {{--                                  Right                                 --}}
        {{-- ---------------------------------------------------------------------- --}}

        <div class="col-md-8">


            <div class="card ">
                <div class="card-body">
                    <div class="card-title font-weight-bold">@lang('Datos basicos') </div>

                    <form wire:submit.prevent="accessAccount(Object.fromEntries(new FormData(event.target)))"
                        autocomplete="off">


                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="name"><span class="text-danger">*</span>@lang('Nombre')</label>
                                    <input id="name" wire:model.defer="name" type="text" placeholder="@lang('Nombre')"
                                        class="form-control @error('name') is-invalid @enderror" id="name" name="name"
                                        value="{{ old('name') }}" required>
                                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lastname"><span class="text-danger">*</span>@lang('Apellido/s')</label>
                                    <input id="lastname" wire:model.defer="lastname" type="text"
                                        placeholder="@lang('Apellido/s')"
                                        class="form-control @error('lastname') is-invalid @enderror" id="lastname"
                                        name="lastname" value="{{ old('lastname') }}" required>
                                    @error('lastname') <span class="text-danger">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                      

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email"><span class="text-danger">*</span>@lang('Correo electrónico')</label>
                                    <input id="email" wire:model.defer="email" type="text"
                                        placeholder="@lang('Correo electrónico')"
                                        class="form-control @error('email') is-invalid @enderror" id="email" name="email"
                                        value="{{ old('email') }}" required>
                                    @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                                </div>
        
                            </div>
                            <div class="col-md-6">
                                <style>
                                    .iti {
                                        width: 100%;
                                    }
                                </style>
        
                                <div class="form-group" wire:ignore>
                                    <label for="phone"><span class="text-danger">*</span>@lang('Teléfono')</label>
                                    <input id="phone" wire:model.defer="phone" type="text"
                                        class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone"
                                        value="{{ old('phone') }}" required>
        
                                    @error('phone') <span class="text-danger">{{ $message }}</span> @enderror
                                </div>
                                <input wire:model.defer="phone_full" type="hidden" name="phone_full" id="phone_full" value="">
        
        
                            </div>
                        </div>

                       


                        
                        

                        <div class="form-group text-right  ">
                            <button type="submit" wire:loading.attr="disabled"
                                class="btn btn-outline-primary  ">

                                <div wire:loading>
                                    <span class="spinner-border spinner-border-sm" role="status"
                                        aria-hidden="true"></span>
                                    Loading...
                                </div>

                                <div wire:loading.remove> Guardar</div>
                            </button>
                        </div>

                    </form>

                </div>
            </div>



            <div class="card mt-3">
                <div class="card-body">
                    <div class="card-title font-weight-bold">Tus datos fiscales</div>


                    <form wire:submit.prevent="datosFiscales(Object.fromEntries(new FormData(event.target)))" autocomplete="off">
       
                    <div class="row ">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="@error('segments') error-select @enderror">
                                    <div>
                                        <label for="segments"> <span class="text-danger">*</span> @lang('Tipo de cliente')</label>

                                        <select x-model="segments" wire:model.defer="segments" name="segments" id="segments"
                                             title="Nombre" class="selectpicker form-control custom-select"
                                            data-size="10" data-width="100%" data-live-search="true">
                                            <option value="">@lang('Seleccione')</option>
                                            <option value="@lang('Particular')">@lang('Particular')</option>
                                            <option value="@lang('Empresa')">@lang('Empresa')</option>
                                            <option value="@lang('Autónomo')">@lang('Autónomo')</option>

                                        </select>
                                    </div>
                                </div>
                                @error('segments') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>

                        </div>


                         


                            <div   x-show="segments === 'Empresa'"   x-transition class="form-group col-md-6">
                                <label for="name_company"><span class="text-danger">*</span>@lang('Razón social')</label>
                                <input x-model="name_company" id="name_company" x-model="name_company" type="text"
                                    placeholder="@lang('Razón social')"
                                    class="form-control @error('name_company') is-invalid @enderror" id="name_company"
                                    name="name_company" value="{{ old('name_company') }}" >
                                @error('name_company') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                       
                    </div>



                    <div class="row">

                        <div class="form-group col-md-6">
                            <label for="identification"><span class="text-danger">*</span>@lang('Identificación fiscal') <span class="font-weight-bold"
                                    x-text="segments === 'Empresa' ? '(CIF)' : '' "></span>  </label>
                            <input id="identification" wire:model.defer="identification" type="text"
                                placeholder="@lang('Identificación fiscal')"
                                class="form-control @error('identification') is-invalid @enderror" id="identification"
                                name="identification" value="{{ old('identification') }}" >
                            @error('identification') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>


                        <div  x-show="segments === 'Empresa'"   class="form-group col-md-6">
                            <label for="intracommunity"> @lang('CIF Intracomunitario') (@lang('Opcional'))</label>
                            <input id="intracommunity" wire:model.defer="intracommunity" type="text"
                                placeholder="@lang('CIF Intracomunitario')"
                                class="form-control @error('intracommunity') is-invalid @enderror" id="intracommunity"
                                name="intracommunity" value="{{ old('intracommunity') }}" >
                            @error('intracommunity') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>



                    </div>

                    <div class="row">

                        <div class="form-group col-md-5">
                            <label for="direction"><span class="text-danger">*</span>@lang('Dirección')</label>
                            <input id="direction" wire:model.defer="direction" type="text"
                                placeholder="@lang('Dirección')"
                                class="form-control @error('direction') is-invalid @enderror" id="direction"
                                name="direction" value="{{ old('direction') }}" >
                            @error('direction') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>

                        <div class="form-group col-md-4">
                            <label for="province"><span class="text-danger">*</span>@lang('Provincia')</label>
                            <input id="province" wire:model.defer="province" type="text"
                                placeholder="@lang('Provincia')"
                                class="form-control @error('province') is-invalid @enderror" id="province"
                                name="province" value="{{ old('province') }}" >
                            @error('province') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="zip"><span class="text-danger">*</span>@lang('Código postal')</label>
                            <input id="zip" wire:model.defer="zip" type="text" placeholder="@lang('Código postal')"
                                class="form-control @error('zip') is-invalid @enderror" id="zip" name="zip"
                                value="{{ old('zip') }}" >
                            @error('zip') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>



                        <div class="form-group">
                            <label for="country"><span class="text-danger">*</span>@lang('País') <span x-bind:class="country2"
                                    style="position: absolute; left: 10px; margin-top: 32px;  margin-left: 18px"></span>
                            </label>
                            <div class="@error('country') error-select @enderror">
                                <div>
                                    <select wire:model.defer="country" style="text-indent: 20px" x-model='country'
                                        name="country" id="country" required title="@lang('País')"
                                        class="  form-control custom-select">

                                        @foreach (Config('pais.pais') as $a => $item)
                                        <option value="{{ $a }}"> {{ $item }}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            @error('country') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>


                      


                    </div>
                    <div class="form-group text-right ">
                        <button type="submit" wire:loading.attr="disabled" class="btn btn-outline-primary">

                            <div wire:loading>
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                Loading...
                            </div>

                            <div wire:loading.remove> @lang('Guardar')</div>
                        </button>
                    </div>

</form>

                </div>
            </div>
        </div>

        {{-- ---------------------------------------------------------------------- --}}


    </div>


    @push('scripts')
    <script src="/assets/plugins/validator-pass/script.js"></script>

    <script>
        function main_account() {
            return {
                open: true,
                country: @entangle('country'),
                segments: @entangle('segments'),
                name_company: @entangle('name_company').defer ,
 
                country2: function () {

                    return 'flag-icon flag-icon-' + this.country.toLowerCase();
                }

            };
        }


        $(function () {
            var input = document.querySelector("#phone")

            var iti = window.intlTelInput(input, {

                utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js",
                initialCountry: "es",

            });


            $("#country").bind('change', function (event) {

                if ($("#phone").val() === '') {
                    iti.setCountry($(this).val().toLowerCase());
                }

            });

            $("#phone").bind('input', function (event) {

                if (iti.isValidNumber()) {

                    $("#phone").addClass('is-valid');
                    $("#phone").removeClass('is-invalid');
                } else {

                    $("#phone").addClass('is-invalid');
                    $("#phone").removeClass('is-valid');

                }
               
                $('#phone_full').val(iti.getNumber());

            })
        })

        document.addEventListener("DOMContentLoaded", () => {
            window.addEventListener('save', (event) => {

                var Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: function (toast) {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: 'success',
                    title: 'Successfully saved'
                })


            })
        })
    </script>
    @endpush
</div>
</div>
