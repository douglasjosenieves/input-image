public function reportSend(Builder $builder)
    {
        $model = Envio::query();
        if (request()->ajax()) {

            $model = Envio::query();
            return Datatables::of($model->with(['user', 'beneficiario'])->orderBy('id', 'desc'))
                ->editColumn('created_at', function ($user) {
                    return $user->updated_at->format('Y/m/d');
                })
                ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
                })

                ->make();

        }

        $html = $builder->columns([
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
            ['data' => 'user.country', 'name' => 'user.country', 'title' => 'Origen'],
            ['data' => 'pais', 'name' => 'pais', 'title' => 'Destino'],
            ['data' => 'user.name', 'name' => 'user.name', 'title' => 'Usuario'],
            ['data' => 'envia', 'name' => 'envia', 'title' => 'Envia'],
            ['data' => 'recibe', 'name' => 'recibe', 'title' => 'Recibe'],
            ['data' => 'tasa', 'name' => 'tasa', 'title' => 'Tasa'],
            ['data' => 'beneficiario.banco', 'name' => 'beneficiario.banco', 'title' => 'Banco'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Created At'],

        ]);

        return view('report-send', compact('html'));
    }
