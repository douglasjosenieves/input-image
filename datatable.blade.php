@extends('voyager::master')

@section('content')
 <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/autofill/2.3.9/css/autoFill.dataTables.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">

<style>
  .dataTables_wrapper .dataTables_paginate .paginate_button {
padding: 0px
  }
  .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
    color: white !important;
    border: none
  }
 
</style>
<div class="page-content">
    @include('voyager::alerts')

    <div class="container">

        <div class="card">
            <div class="card-body">
                <div class="card-header">
                    <h3>Reporte de envios</h3>
                </div>
                <div class="row">


                  <div class="mt-5 responsive" style="margin-top: 20px">

                     {!! $html->table(['class' => 'table table-bordered'], true) !!}
                  </div>


                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')

<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>

{{-- {!! $html->scripts() !!} --}}

<script>
  $(function () {
      window.LaravelDataTables = window.LaravelDataTables || {};
      window.LaravelDataTables["dataTableBuilder"] = $("#dataTableBuilder").DataTable({
          "serverSide": true,
          "processing": true,
          "ajax": "",
          "pageLength": 50,
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],     // page length options
          "dom": "Bfrtip",
           "buttons": [
          'copy', 'csv', 'excel', 'pdf', 'print', 'pageLength'
            ],
          "columns": [{
              "name": "id",
              "data": "id",
              "title": "Id",
              "orderable": true,
              "searchable": true
          }, {
              "name": "user.country",
              "data": "user.country",
              "title": "Origen",
              "orderable": true,
              "searchable": true
          }, {
              "name": "pais",
              "data": "pais",
              "title": "Destino",
              "orderable": true,
              "searchable": true
          }, {
              "name": "user.name",
              "data": "user.name",
              "title": "Usuario",
              "orderable": true,
              "searchable": true
          }, {
              "name": "envia",
              "data": "envia",
              "title": "Envia",
              "orderable": true,
              "searchable": true
          }, {
              "name": "recibe",
              "data": "recibe",
              "title": "Recibe",
              "orderable": true,
              "searchable": true
          }, {
              "name": "tasa",
              "data": "tasa",
              "title": "Tasa",
              "orderable": true,
              "searchable": true
          }, {
              "name": "beneficiario.banco",
              "data": "beneficiario.banco",
              "title": "Banco",
              "orderable": true,
              "searchable": true
          }, {
              "name": "created_at",
              "data": "created_at",
              "title": "Created At",
              "orderable": true,
              "searchable": true
          }]
      });
  });
</script>


<script>
  
</script>
 
@stop
